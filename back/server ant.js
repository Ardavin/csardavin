var express = require('express');
var cors =require('cors');
var bcrypt = require('bcrypt-nodejs');

var jsonwebtoken = require('jsonwebtoken');
//var task = require('.user/todolitsmodel.js')

  app = express();
  app.use(cors());

  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=kE1Ef_8i9fSFXF-hpDHQsK9GLT6HYKxZ";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);


//CONEXION CON POSTGRE
var pg =require('pg');

var urlusuarios = "postgres://docker:docker@localhost:5433/bdseguridad"//ejecutandolo desde fuera del contenedor.

//ojo que dependen si lo ejecutamos desde el cntentedo o no.
//var urlusuarios = "postgres://docker:docker@serverpostgre:5432/bdseguridad

app.use(function(req, res, next) {
  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
    jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function(err, decode) {
      if (err) req.user = undefined;
      req.user = decode;
      next();
    });
  } else {
    req.user = undefined;
    next();
  }
});


var routes = require('./user/todolistroutes.js');
routes(app);


app.use(function(req, res) {
  res.status(404).send({ url: req.originalUrl + ' not found' })
});




//DATOS CONEXION A MONGODB
var MongoClient =require('mongodb').MongoClient;
var url_mongo = "mongodb://localhost:27017/local"; //si es local, localhost, pero si es en contenedor el nombre de la maquina dnde esta cn la misma red

//var url = "mongodb://servermongo:27017/local"

//FIN DATOS CONEXION A MONGODB





app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// app.get('/movimientos', function(req, res) {
//   //res.sendFile(path.join(__dirname, 'index.html'));
//     clienteMlab.get('', function(err, resM, body) {
//       if(err)
//       {
//         console.log(body);
//       }
//       else
//       {
//         res.send(body);
//       }
//     });
// });
//
// app.get('/clientes', function(req, res) {
//   //res.sendFile(path.join(__dirname, 'index.html'));
//     clienteMlab.get('&f={"idcliente":1, "nombre": 1, "apellidos": 1}', function(err, resM, body) {
//       if(err)
//       {
//         console.log(body);
//       }
//       else
//       {
//         res.send(body);
//       }
//     });
// });
//
// app.post('/movimientos', function(req, res) {
//   /* Crear movimiento en MLab */
//
// });
//
//
//
//
//
//
//
// app.get('/movimientos/:idcliente', function (req, res) {
//   /* Obtener movimientos del cliente idcliente desde MLab */
// })
//
//
//
// //FUNCION QUE COMPRUEBA EL LOGIN DE LOS USUARIOS
// app.post('/login',function(req,res){
//    //Creamos el cliente
//    var clientePostgre = new pg.Client(urlusuarios);
//    //Conectamos
//    clientePostgre.connect();
//    var resultado=0;
//    var acceso;
//    var tipo;
//    //Hacemos la consulta para conocer si existe el cliente
//
//    //Es necesario o bien encriptar de nuevo
//
//
//    const query =clientePostgre.query('SELECT * FROM usuarios WHERE login=$1 AND password=$2;',[req.body.usuario,req.body.password], function (err,result){
//    if (err){
//       console.log(err);
//       // Enviamos error y capuramos siguiente paso enviar el err.
//       acceso=false;
//       tipo='';
//    }else{
//      //resul.rows[0] son los ersultados, aunque sea un count, siempre lo envian en filas.
//       console.log(result.rows[0]);
//       //hay que capturar los posibles errores.
//       //LOGICA PARA CONOCER SI ES VALIDO EL USUARIO O NO
//       //conocer si la consulta es vacía
//       if(result.rows[0] === undefined){
//          // El usuario y/o el password no existen con un usuario valido.
//           acceso=false;
//           tipo='';
//       }else{
//          //el usuario y password es correcto.
//           var usuario = result.rows[0].login;
//           var pass = result.rows[0].password;
//           tipo = result.rows[0].tipo;
//           acceso=true;
//         }
//     }
//     //Devolvemos el resultado del login
//     respuesta_json={"acceso":acceso,"tipo":tipo};
//     res.send(respuesta_json);
// })
// })
// //*** FIN FUNCION QUE COMPRUEBA EL LOGIN DE LOS USUARIOS
//
//
//
// //FUNCION QUE RECUPERA LAS CUENTAS DEL USUARIOS
// app.get('/mongocuentas/:id_cliente', function(req, res) {
//   //Montamos el filtro por el que queremos buscar.
//   console.log(req.params.id_cliente);
//   var consulta_json={"usuario":req.params.id_cliente}
//   //Concectamos con la base de datos.
//   MongoClient.connect(url_mongo,function(err,db){
//       if (err){
//         console.log(err);
//       }else{
//           //Buscamos en la collección cuentas, las asociadas a id_cliente
//           var col=db.collection('cuentas');
//           col.find(consulta_json).limit(3).toArray(function(err,docs){
//           res.send(docs);
//           console.log(docs);
//           });
//           db.close();
//       }
//
// });
// })
// //**FIN FUNCION QUE RECUPERA LAS CUENTAS DEL USUARIOS
//
//
//
// //FUNCION QUE RECUPERA LOS MOVIMIENTOS DEL USUARIOS
// app.get('/mongomovimientos/:id_cuenta', function(req, res) {
//   //Montamos el filtro por el que queremos buscar.
//   console.log(req.params.id_cuenta);
//   var consulta_json={"cuenta":req.params.id_cuenta}
//   //Concectamos con la base de datos.
//   MongoClient.connect(url_mongo,function(err,db){
//       if (err){
//         console.log(err);
//       }else{
//           //Buscamos en la collección cuentas, las asociadas a id_cliente
//           var col=db.collection('movimientos');
//           col.find(consulta_json).limit(3).toArray(function(err,docs){
//           res.send(docs);
//           console.log(docs);
//           });
//           db.close();
//       }
//
// });
// })
// //**FIN FUNCION QUE RECUPERA LOS MOVIMIENTOS DEL USUARIOS
//
// //FUNCION QUE CREA UN NUEVO USUARIOS EN POSTGRES
// app.post('/usuarios', function(req, res) {
//   /* Crear usuario en postgres*/
//   var clientePostgre = new pg.Client(urlusuarios);
//   //Conectamos
//   clientePostgre.connect();
//   //insert into usuarios (nombre, clave) values ('Mariano','payaso');
//
//   bcrypt.hash(req.body.password, null, null, function(err, hash) {
//       // Store hash in your password DB.
//       if (err){
//
//       }else{
//         const query =clientePostgre.query('INSERT INTO usuarios (login,password) VALUES ($1,$2);',[req.body.usuario,hash], function (err,result){
//           if (err){
//              console.log(err);
//              // Enviamos error y capuramos siguiente paso enviar el err.
//           }else{
//             //resul.rows[0] son los ersultados, aunque sea un count, siempre lo envian en filas.
//              console.log(result.rows[0]);
//            }
//          })
//       };
//
//   });
// });
// //** FIN FUNCION QUE CREA UN NUEVO USUARIOS EN POSTGRES
//
//
//
// //FUNCION QUE VALIDA EL PASSWORD UNA VEZ RECUPERADO
// function validapassword(password,hash){
//
//   bcrypt.compare(password, hash, function(err, res) {
//       // res == true
//   });
//
// }
// //** FIN FUNCION QUE ENCRIPTA EL PASSWORD ANTES DE AMACENARLO
