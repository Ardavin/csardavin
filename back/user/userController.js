var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');


var pg =require('pg');
var urlusuarios = "postgres://docker:docker@localhost:5433/bdseguridad"//ejecutandolo desde fuera del contenedor.


exports.register = function(req, res){
   //Encriptamos el passwor del usuario al registrarse
   var clientePostgre = new pg.Client(urlusuarios);
   //Conectamos
   clientePostgre.connect();

 console.log(req.body);
   var salt = bcrypt.genSaltSync(10);
   var hash_password = bcrypt.hashSync(req.body.password, salt);
  // meter un callback a esta funcion porque puede que no la hata creado
   console.log(hash_password);

   //salvamos los datos del usuario nuevo.

   const query =clientePostgre.query('INSERT INTO usuarios (login,password,tipo,imagen_usuario,nombre,apellidos) VALUES ($1,$2,$3,$4,$5,$6);',[req.body.login,hash_password,req.body.tipo,req.body.avatar,req.body.nombre,req.body.apellidos], function (err,result){
     if (err){
        console.log(err);
        return res.status(400).send({
        message: err });
        // Enviamos error y capuramos siguiente paso enviar el err.
     }else{
       //resul.rows[0] son los ersultados, aunque sea un count, siempre lo envian en filas.
      //  console.log("resultado" + result.rows[0]);
        //return res;
        return res.json({ message: 'usuario creado.' });
      }
 });


 }
//**FIN FUNCIÓN REGISTRO

exports.sign_in = function(req, res){

  var clientePostgre = new pg.Client(urlusuarios);
  //Conectamos
  clientePostgre.connect();
  var resultado=0;
  var acceso;
  var tipo;

  console.log("estamos en el login");
  console.log("login"+ req.body.usuario);

  const query =clientePostgre.query('SELECT * FROM usuarios WHERE login=$1;',[req.body.usuario], function (err,result){
  if (err){
     console.log(err);
     // Enviamos error y capuramos siguiente paso enviar el err.
     acceso=false;
     tipo='';
  }else{
    //resul.rows[0] son los ersultados, aunque sea un count, siempre lo envian en filas.
     console.log(result.rows[0]);
     //hay que capturar los posibles errores.
     //LOGICA PARA CONOCER SI ES VALIDO EL USUARIO O NO
     //conocer si la consulta es vacía
     if(result.rows[0] === undefined){
        // El usuario  no existe con un usuario valido.
         acceso=false;
         tipo='';
         res.status(401).json({ message: ' 0 Authentication failed. User and or  authentication not found.' });
     }else{
          var pass = result.rows[0].password;
          var pass_2 =  req.body.password;
          console.log(pass + " y " + pass_2);


          if (!bcrypt.compareSync(req.body.password,pass )){
            res.status(401).json({ message: '  authentication not found en compare.' });
          }else{
            //el usuario y password es correcto.
            var usuario = result.rows[0].login;
            var pass = result.rows[0].password;
            tipo = result.rows[0].tipo;
            img_usr = result.rows[0].imagen_usuario;
            acceso=true;
            //     return res.json({token: jwt.sign({ login: usuario, password: pass}, 'RESTFULAPIs')});
          return res.json({token: jwt.sign({ login: usuario, password: pass}, 'RESTFULAPIs'),usuario:usuario, acceso:acceso,tipo:tipo,imagen_usuario:img_usr});
          }

       }
}
});



}

//FUNCIÓN QQUE COMPRUEBA EL LOGIN
exports.loginRequired = function(req, res,  next){
  console.log('3 ' + req.user);
  console.log("cuerpo" + JSON.stringify(req.body));
  console.log("cabeceras" + JSON.stringify(req.headers));
  console.log("aut" + req.headers.authorization);
   if (req.user) {
      next();
    } else {
      console.log('4 ' + req.user);
    return res.status(401).json({ message: 'Unauthorized user en loginRequired!' });
}
}
//*** FIN FUNCION QUE COMPRUEBA EL
