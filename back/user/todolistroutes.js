

module.exports = function(app){
  var todoList = require('./todolistControllers.js'),
  userHandlers = require('./userController.js');


  	// todoList Routes




  	app.route('/auth/register')
  		.post(userHandlers.register);

  	app.route('/auth/sign_in')
  		.post(userHandlers.sign_in);

    app.route('/clientes')
      .get(userHandlers.loginRequired, todoList.estoy_en_clientes);

      app.route('/mongocuentas/:id_cliente')
        .get(userHandlers.loginRequired, todoList.carga_cuentas_cliente);


          //revisar esto que no esta bien conceptualmente
        app.route('/movimientos/:id_cuenta')
          .get(userHandlers.loginRequired, todoList.carga_movimientos_cuenta)
          .post(userHandlers.loginRequired, todoList.actualiza_movimientos_cuenta);

    app.route('/cuentas/:id_cuenta/movimientos')
          .post(userHandlers.loginRequired, todoList.crea_movimientos_cuenta);

        app.route('/usuarios')
          .get(userHandlers.loginRequired, todoList.consulta_lista_usuarios)
          .post(userHandlers.loginRequired, userHandlers.register);


        app.route('/usuarios/:id_usuario')
            .get(userHandlers.loginRequired, todoList.consulta_usuarios)
            .delete(userHandlers.loginRequired, todoList.borra_usuario);


        app.route('/clientes_externos/:id_cliente/cuentas/')
                .get(userHandlers.loginRequired, todoList.consulta_cuentas_externas);


        app.route('/cuentas_externas/:id_cuenta/movimientos_externos')
                    .get(userHandlers.loginRequired, todoList.consulta_movimientos_externos);







      app.route('/administradores/:id_administrador')
        .get(userHandlers.loginRequired, todoList.consulta_usuarios);

        app.route('/buckets')
        .get(todoList.lista_buckets);

        app.route('/documentos')
        .get(todoList.lista_documentos)
        .post(todoList.add_documento);


       app.route('/recibetokentarjetas')
       .get(todoList.movimientos_tarjeta);

       app.route('/tarjetas')
       .get(todoList.recupera_movimientos_tarjeta);


  };
