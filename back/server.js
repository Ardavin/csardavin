var express = require('express');
var cors =require('cors');




  app = express();
  app.use(cors());

  port = process.env.PORT || 3000,
  bodyParser = require('body-parser'),
  jsonwebtoken = require("jsonwebtoken");


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use(formidable());


app.use(function(req, res, next) {

  console.log(next);
  console.log(req.body);

  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
    jsonwebtoken.verify(req.headers.authorization.split(' ')[1], 'RESTFULAPIs', function(err, decode) {
      console.log('1 ' + decode);
      if (err) req.user = undefined;

      req.user = decode;
      console.log('2 ' + req.user);
      next();
    });
  } else {
    req.user = undefined;
    next();
  }
});

var routes = require('./user/todolistroutes.js');
routes(app);


app.use(function(req, res) {
  console.log(req.originalUrl);
  res.status(404).send({ url: req.originalUrl + ' pagina not found' });
});

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

module.exports = app;
